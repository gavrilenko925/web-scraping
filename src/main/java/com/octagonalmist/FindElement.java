package com.octagonalmist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class FindElement {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.gecko.driver",
                "D:\\Users\\Gavri\\Desktop\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://rozetka.com.ua/");
        List<WebElement> textDemo = driver.findElements(By.xpath("//*[contains(text(),'Доставка по всей Украине')]"));
        System.out.println("Number of web elements: " + textDemo.size());
        if (textDemo.size() >= 1) {
            System.out.println("Такой текст на сайте есть!");
        } else {
            System.out.println("На сайте нет такого текста");
        }
        driver.quit();
    }
}
